variable "ami_id" {
  type = string
  default = "ami-079b5e5b3971bd10d" 
}
variable "type" {
    type = list(string)
    description = "instance type"
    default = ["t2.micro","t3.micro","t2.micro"] 
#                 0           1       2
}
variable "tags" {
  type = map(string)
  description = "thi is map"
  default = {
    Name = "instanse testing"
    Env = "dev"
    App = "test-app"
    Account = "sample"
  } 
}
# Bool Vaue
variable "public_ip" {
  type = bool
  default = false
  }