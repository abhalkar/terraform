provider "aws" {
  profile = "akshay"
}
resource "aws_instance" "ec1" {
  ami = var.ami_id
  instance_type = var.type[0]
  tags = var.tags
  associate_public_ip_address = var.public_ip
}
resource "aws_instance" "ec2" {
  ami = var.ami_id
  instance_type = var.type[1]
  tags = var.tags
  associate_public_ip_address = var.public_ip
}
resource "aws_instance" "ec3" {
  ami = var.ami_id
  instance_type = var.type[2]
  tags = var.tags
  associate_public_ip_address = var.public_ip
}