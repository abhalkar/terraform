# terraform 
- terraform is used for creating the infra on multiple platorm such as aws,azure,gcp .
- terraform supports multi cloud platform 
- The script is writeen in HrshiCrop lanuage with the extesion main.tf
- commands used in terra form 
    - terraform init   -> to initilize the dir so all pkg of aws will get in dir
    - terraform plan   -> Here we will seehow our infra will look like
    - terraform apply  -> make the actul changes on infra
        - terrafrom apply -auto-approve 
    - terraform destory  -> to destory all infra created by terraform
    - terraform fmt main.tf -> main HCl script file
## how terrafrom work 
- Write
    - Write terrafrom script
- plan 
    - will show show wows our paln 
- apply 
    - will apply script on env

## Terraformm has its own memory lets see
- when we apply any code it get saved in .tfstate and in backup.tfstate
- tfstate will store all the changes made and the configuration all info will present here.
- if we delete the tfstate file then it will take bachup form backup.tfstate
- and if delete backup.tfstate file as well then it will sart from new cretion all the old infra will stay as it is because we had deleted the memeory of terraform.

## How to avoid provideing secret key and access key 
- we can run
    - aws configuration      -> so that profile will br created in ~/.aws/creds 
        - aws configure --profil akshya   -> so will save
- We need to provide the profile in terraform script
    - for eg: 
    --- provider "aws" {
             profile = "akshay"
            }
        resource "aws_instance" "ec2" {
        ami = "ami-079b5e5b3971bd10d"
        instance_type = "t2.micro"
        tags = {
            Name = "ec-lamp"
        }
        } 
    ...
# what is varible in terraform 
- we can use single varible in multiple places so if we want to change the value we can  chamge in single plce will affect at multiple location.
- we will create a seprate file for vars.tf name in which TYPE is compulsary we provide valuw in form of 
    - key = vale 
      varible "ami" {
         type = string 
     }
    - $ variables 
        - string
            - key = value
        - list
            - list = [1,2,3,4]
        - map (multiple values)
            - {
                a = "1"
                b = "2"
                c = "3"
            } 
        - bool 
            - key = true/false
# Lets see how to generate output
-     